<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ifbtn');

/** MySQL database username */
define('DB_USER', 'ifbuser');

/** MySQL database password */
define('DB_PASSWORD', 'eXxIEAWFZfnIHawI');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8lN@~`0-F::{LkRpnyXE`hJ5s=h@te*bX|XIW$66}dO2Mb<#4m@)U}5E:`j_h=1e');
define('SECURE_AUTH_KEY',  'uX[7c9oKw4PQr3]K|A)%2uv0uGsr=@yngb,q~}C^WaKEHaH3+k{z~pZ1Px>`)F8;');
define('LOGGED_IN_KEY',    'fU>Fv&~;IA?rCpfv:W]vE*Zegl0rOoYEi~%dWAa[Otk>1u>)[AcJe24tM>WZL*I!');
define('NONCE_KEY',        '-@K#=]mpu:9vZ_<i^3, =25lLMMj%I9[1F=v#OXM+aS%P/?%N5lVT@Vhq$:F$-d(');
define('AUTH_SALT',        'gb@6Aav?;M]},sUYT#|PVExnXK_R`Lc#-qt^_ p2),ryq%7 <un^q5$XvF>,BdPv');
define('SECURE_AUTH_SALT', ')d_wqQFm -e`G7r&`mM@d. LT er$DoUAQ^I=iIiqPrvpTcs2Z>q] u(}M[TsMaq');
define('LOGGED_IN_SALT',   'Q*3<1&1-m.9N]=z6PDN$=#JbYg=Yl|5*cTHlw]UOR$i(MCb^Q0d}>KzG7xW:B.u9');
define('NONCE_SALT',       '@C1D=@-$YGhL+#SS*<,xISHgV]V;~I]4*JA-(d)uv08>%8ATrsSce_$|)y%#gv~C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
